import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Prove';
  textOfMessage?: string;
  emailUserValid: boolean = false;
  checkboxChecked: boolean = false;
  enabledButton: string = ''

  constructor(private service: AppService) {}

  ngOnInit() {
    
  }
  getTheChecked(event: boolean) {
    this.checkboxChecked = event
    this.getEnabledButton()
  }
  getEmail(event: boolean) {
    this.emailUserValid = event
    this.getEnabledButton()
  }
  getEnabledButton(){
    const emailValid = this.emailUserValid
    const checkboxValid = this.checkboxChecked
    if (emailValid === true && checkboxValid === true){
      this.enabledButton = 'true'
    } else {
      this.enabledButton = 'false'
    }
  }
  message(event: string): void {
    this.textOfMessage = event
  }
}