import { Component, EventEmitter, Input, OnInit, OnChanges, SimpleChanges, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit, OnChanges {
  @Input() enabled?: string
  @Output() messageEvent: EventEmitter<string> = new EventEmitter<string>()

  disabled: boolean = true;


  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes.enabled.currentValue)
    if (changes.enabled.currentValue === 'true') {
      this.disabled = false
    } else {
      this.disabled = true
    }
  }

  ngOnInit(): void {
  
  }
  onClick(): void {
    this.messageEvent.emit('¡Perfecto, te avisaremos la primera!')
  }
  enabledButton(): void {
  }
}
