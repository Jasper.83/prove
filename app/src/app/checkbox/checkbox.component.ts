import { Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent implements OnInit {
  @Output() checkboxEvent: EventEmitter<boolean> = new EventEmitter<boolean>()
  

  constructor() { }

  ngOnInit(): void {
  }
  checkboxValue(event:any) {

    this.checkboxEvent.emit(event.target.checked)

  }

}
