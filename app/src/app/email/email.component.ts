import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})

export class EmailComponent implements OnInit {
  @Output() emailValueEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  placeholder: string = 'usuario@dominio.com'
  emailContent: string = ''

  constructor() { }

  ngOnInit(): void {
  }
  sendEmail(): void{
    const regularExpresion = /\S+@\S+\.\S+/;
    const isEmailValid = regularExpresion.test(this.emailContent);
    
    this.emailValueEvent.emit(isEmailValid)
  
  }
}

